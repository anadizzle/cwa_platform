require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
	test "full title helper" do
		assert_equal full_title, "CWA Build v0.1"
		assert_equal full_title("Contact"), "Contact | CWA Build v0.1"
	end
end