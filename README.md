# CWA Build v0.1

## Getting started

To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install --without production
```

Next, migrate the database:

```
$ rails db:migrate
```

Finally, run the test suite to verify that everything is working correctly:

```
$ rails test
```

If the test suite passes, you'll be ready to run the app in a local server:

```
$ rails server
```

For more information, contact me at anadi_m@outlook.com

```
$ Heroku Live Demo
```

https://quiet-tor-44615.herokuapp.com/